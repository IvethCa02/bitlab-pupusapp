package sv.edu.bitlab.pupusap

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_detalle_orde.*
import java.text.DecimalFormat


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
/*
private const val ARG_PARAM3 = "param3"
private const val ARG_PARAM4 = "param4"
 */

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PruebaFragment.PruebaFragmentListener] interface
 * to handle interaction events.
 * Use the [PruebaFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PruebaFragment : Fragment() {
  // TODO: Rename and change types of parameters
  private var param1: String? = null
  private var param2: Int? = null
  /*
  private var param3: Int? = null
  private var param4: Int? = null
   */
  private var arroz: ArrayList<Int>? = null
  private var maiz: ArrayList<Int>? = null
  private var listener: OnFragmentInteractionListener? = null
  //private var handler: Handler = Handler()
  

  val idItem = arrayOf(
    arrayOf(R.id.lineItemDetail1, R.id.lineItemPrice1),
    arrayOf(R.id.lineItemDetail2, R.id.lineItemPrice2),
    arrayOf(R.id.lineItemDetail3, R.id.lineItemPrice3),
    arrayOf(R.id.lineItemDetail4, R.id.lineItemPrice4),
    arrayOf(R.id.lineItemDetail5, R.id.lineItemPrice5),
    arrayOf(R.id.lineItemDetail6, R.id.lineItemPrice6)
  )

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    arguments?.let {
      param1 = it.getString(ARG_PARAM1)
      param2 = it.getInt(ARG_PARAM2)
     /* param3 = it.getInt(ARG_PARAM3)
      param4 = it.getInt(ARG_PARAM4)
      */
      arroz = it.getIntegerArrayList(CONTADOR_ARROZ)
      maiz = it.getIntegerArrayList(CONTADOR_MAIZ)
    }
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {

    return inflater.inflate(R.layout.fragment_prueaba, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    displayDetalle()

    val confirmarOrden = this.view!!.findViewById<Button>(R.id.confirmarOrden)
    confirmarOrden.setOnClickListener {
      val containerView: View? = activity?.findViewById(R.id.containerViews)
      containerView!!.visibility = View.VISIBLE


    }

    Log.d("Arroz Cantidad", "$arroz")
    Log.d("Maiz Cantidad", "$maiz")

   /* val boton = this.view!!.findViewById<Button>(R.id.button)
       button.setOnClickListener {
         val pantalla: View? = FrameLayout.findViewById(R.id.pantallaverde)
         pantalla!!.visibility = View.VISIBLE
    }

    */
  }

  fun displayDetalle() {
    val arr = arroz!! + maiz!!
    var total = 0.0f
    for((index, contador) in arr.withIndex()){
      val ids = idItem[index]
      val detailTexview = this.view!!.findViewById<TextView>(ids[0])
      val priceTextView= this.view!!.findViewById<TextView>(ids[1])
      if(contador > 0){
        val totalUnidad = contador * VALOR_PUPUSA
        val descripcion = getDescripcion(index)
        detailTexview.text = getString(R.string.pupusa_line_item_description,
          contador, descripcion)
        total += totalUnidad
        val precio = DecimalFormat("$#0.00").format(totalUnidad)
        priceTextView.text = precio
      } else{
        detailTexview.visibility = View.GONE
        priceTextView.visibility = View.GONE
      }
    }
    val totalPrecio = this.view!!.findViewById<TextView>(R.id.lineItemPriceTotal)
    val precio = DecimalFormat("$#0.00").format(total)
    totalPrecio.text = precio
  }

  fun getDescripcion(index: Int): String {
    return when(index){
      QUESO -> "Queso de arroz"
      FRIJOLES -> "Frijol con queso de arroz"
      REVUELTAS -> "Revueltas de arroz"
      QUESO_MAIZ-> "Queso de maiz"
      FRIJOLES_MAIZ -> "Frijol con queso de maiz"
      REVUELTAS_MAIZ -> "Revueltas de maiz"
      else -> throw RuntimeException("Pupusa no soportada")
    }
  }

  // TODO: Rename method, update argument and hook method into UI event
  fun onButtonPressed(uri: Uri) {
    listener?.onFragmentInteraction(uri)
  }

  override fun onAttach(context: Context) {
    super.onAttach(context)
    if (context is OnFragmentInteractionListener) {
      listener = context
    } else {
      throw RuntimeException(context.toString() + " must implement PruebaFragmentListener")
    }
  }

  override fun onDetach() {
    super.onDetach()
    listener = null
  }

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   *
   *
   * See the Android Training lesson [Communicating with Other Fragments]
   * (http://developer.android.com/training/basics/fragments/communicating.html)
   * for more information.
   */
  /*interface PruebaFragmentListener {
    // TODO: Update argument type and name
    fun onFragmentInteraction(uri: Uri)
  }

   */

  interface OnFragmentInteractionListener {

    fun onFragmentInteraction(uri: Uri)
  }

  companion object {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PruebaFragment.
     */
    const val QUESO = 0//3
    const val FRIJOLES = 1//4
    const val REVUELTAS = 2//5
    const val QUESO_MAIZ = 3//3
    const val FRIJOLES_MAIZ = 4//4
    const val REVUELTAS_MAIZ = 5//5
    const val CONTADOR_ARROZ = "ARROZ"
    const val CONTADOR_MAIZ = "MAIZ"
    const val VALOR_PUPUSA = 0.5F

    // TODO: Rename and change types and number of parameters

    @JvmStatic
    fun newInstance(
      param1: String, param2: String, arrayArroz: ArrayList<Int>, arrayMaiz: ArrayList<Int>) =
      PruebaFragment().apply {
        arguments = Bundle().apply {
          putString(ARG_PARAM1, param1)
          putString(ARG_PARAM2, param2)
          putIntegerArrayList(CONTADOR_MAIZ, arrayMaiz)
          putIntegerArrayList(CONTADOR_ARROZ, arrayArroz)
         /* putInt(ARG_PARAM3, param3)
          putInt(ARG_PARAM4, param4)
          putInt(ARG_PARAM5, param5)
          putInt(ARG_PARAM6, param6)

          */

        }
      }
  }
}
